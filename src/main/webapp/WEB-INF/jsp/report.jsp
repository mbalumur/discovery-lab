<%@ include file="header.jsp"%>

<body class="startpage">
	<div class="wrap">
		<div class="row header-profile">
			<div class="col-md-6 col-sm-3 col-xs-3 header-left">
				<strong>Discovery</strong>
			</div>
			<div class="col-md-6 col-sm-9 col-xs-9 header-right">
					<a href="${pageContext.request.contextPath}/HomePage">
						<span class="icon-small icon-home icon-effect"></span></a> 
					<a href="#" id="user" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-title="<strong>${sessionScope.user.name}</strong><br>(${sessionScope.user.email})" data-content='<div style="text-align:center; "><button class="bttn-small" style="text-decoration:none; background:#47a3da;" class="bttn-small md-trigger" data-toggle="modal" data-target="#myModal1">Sign out</button> </div>'><span class="icon-small icon-user icon-effect" ></span></a>
			</div>
		</div>
		<div style="padding: 0.5em 5em; background: white; text-align:left">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-xs-12"><h1><span class="icon-file"> </span>Report</h1><br></div>
					<div class="col-sm-6 hidden-xs"><h1 style="float:right">MohanRaj <span class="icon-user"> </span></h1><br></div>
				</div>
			</div>
		</div>

			<section class="report-section">
				<div class="container">
					
					<div class="row">
						<div class="col-sm-6">
						<strong style="color:#47a3da">Personal Information</strong>
						<hr>
						<div class="row">
							<div class="col-sm-3">
								<p></span>Name</p>
							</div>
							<div class="col-sm-9">
								<p>${sessionScope.user.name}</p>
							</div>
						</div>
						</div>
						<div class="col-sm-6">
						<strong style="color:#47a3da">Lab Information</strong>
						<hr>
						<div class="row">
							<div class="col-sm-3">
								<p></span>Title</p>
							</div>
							<div class="col-sm-9">
								<p>Estimating order of magnitude</p>
							</div>
						</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row animated bounceIn">
						<div class="col-sm-4" style="text-align:center">
						<p style="padding:0 0 0.4em 0;"><strong>ATTEMPTED</strong></p>
						<div id="myStat1"  data-text="${attemptperc}%" data-info="(${attempted})" data-width="30" data-fontsize="30" data-percent="${attemptperc}" data-fgcolor="#47a3da" data-bgcolor="#eee" style="margin:0 auto; width:100%;"></div>
						</div>
						<div class="col-sm-4" style="text-align:center">
						<p style="padding:0 0 0.4em 0;"><strong>SCORE</strong></p>
						<div id="myStat2"  data-text="${totalscoreperc}%" data-info="(${totalscore})" data-width="30" data-fontsize="30" data-percent="${totalscoreperc}" data-fgcolor="#fbc73b" data-bgcolor="#eee" style="margin:0 auto; width:100%;"></div>
						</div>
						<div class="col-sm-4" style="text-align:center">
						<p style="padding:0 0 0.4em 0;"><strong>AVG TIME</strong></p>
						<div id="myStat3"  data-text="${avgtime}" data-info="SEC" data-width="30" data-fontsize="30" data-percent="${avgtimeperc}" data-fgcolor="#f06060" data-bgcolor="#eee" style="margin:0 auto; width:100%;"></div>
						</div>
					</div><br>
					<hr>
					<p class="animated bounceInUp" style="font-size:4em; text-align:center; padding: 0.5em 0 0 0;">${msg}!</p>
					<br>
				</div>
			</section>
	

		<footer class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-6 colmd-6"><p style="text-align:left">
				&copy; 2014 Discovery, Inc. &middot; <a href="#">Privacy</a>
				&middot; <a href="#">Terms</a>
			</p></div>
					<div class="col-xs-6 colmd-6"><p style="text-align:right"><a href="#">Back to top</a></p></div>
				</div>
			</div>
			
		</footer>

		<!--Modal-->
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<p>
							<span class="iconwhite-small icon-user"></span> Profile
						</p>
						<hr>
						<div class="profile">
							<p>Do you want to sign out?</p>
						</div>
					</div>
					<div class="modal-footer">
						<a href="${pageContext.request.contextPath}/SignOut" class="bttn" style="text-decoration:none;">Sign out</a>
						<button class="bttn" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

	</div>
		<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="<spring:url value="/resources/js/jquery.circliful.min.js" />"></script>
		
		<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
		
		<script type="text/javascript">

			$( document ).ready(function() {
				$('#myStat1').circliful();
				$('#myStat2').circliful();
				$('#myStat3').circliful();
				$('#user').popover();
		    });

		</script>
	<%@ include file="footer.jsp"%>