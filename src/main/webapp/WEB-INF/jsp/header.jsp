<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Discovery Lab</title>
		<meta name="description" content="An interactive Lab with numerous questions that helps honing the skills of the students" />
		<meta name="keywords" content="interactive, Lab, questions, skills, students" />
		<meta name="author" content="MohanRaj" />
		<link rel="shortcut icon" href="<spring:url value="/resources/icon/favicon.ico" />">
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/normalize.css" />" />
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap.min.css" />" />
		<link rel="stylesheet" type="text/css"	href="<spring:url value="/resources/css/animate.css" />" />
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/icons.css" />" />
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/component.css" />" />
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/media-queries.css" />" />
		<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/jquery.circliful.css" />" />
		<script src="<spring:url value="/resources/js/modernizr.custom.js" />"></script>
		<SCRIPT type="text/javascript">
		    window.history.forward();
		    function noBack() { window.history.forward(); }
		</SCRIPT>
	</head>
	<div class="loadingpage">
			<div class="cont">
			    <div class="spinner">
				  <div class="bounce1"></div>
				  <div class="bounce2"></div>
				  <div class="bounce3"></div>
				</div>
			</div>
		</div>