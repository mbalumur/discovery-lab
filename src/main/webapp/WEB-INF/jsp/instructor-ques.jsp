<%@ include file="header.jsp"%>
		<body class="instructor-page">
	<div class="wrap">

		<header>
			<div class="container-fluid">
				<div class="row header-profile">
					<div class="col-md-6 col-sm-3 col-xs-3 header-left"><a href="index.html" style="color: #fff; text-decoration:none;"><strong>DISCOVERY</strong></a></div>
					<div class="col-md-6 col-sm-9 col-xs-9 header-right">
						<a href="#"><span class="icon-small icon-home icon-effect" ></span></a>
						<a href="#" id="user" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-title="<strong>MohanRaj Balumuri</strong>" data-content='<div style="text-align:center; "><a href="#" class="bttn-small" style="text-decoration:none; background:#47a3da;" class"md-trigger" data-toggle="modal" data-target="#myModal1">Sign out</a> </div>'><span class="icon-small icon-user icon-effect" ></span></a>
					</div>
				</div>
			</div>
		</header>

		<div class="instructor-header">
			<div class="container-fluid">
				<div class="row">
					<div>
						<span><p><strong>Instructor's Page</strong></p></span>
						<p>Please enter the question and answers for labs</p>
						
					</div>
				</div>
			</div>
		</div>
		<div style="padding:0.25em; background: rgba(0,0,0,0.05); width:100%;"></div>
		

		<section class="instructor-content">
			<form:form method="post" action="${pageContext.request.contextPath}/Admin/Submit">
			<div class="container-fluid" id="quesanssection">
				<div class="row">
					<div class="col-sm-6">
						<div class="instructor-innercontent">
							<span><p>Please select the Lab:</p></span>
							<select class="instrcutor-selectbox" name="lab">
								<option value="1">Estimating Order of Magnitude</option>
								<option value="2">What does it take to make water?</option>
								<option value="3">Solar Insolence</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="instructor-innercontent">	
							<span><p>Please enter the Question Number</p></span>
							<input type="text" name="quesno" class="instrcutor-inputbox" placeholder="Enter Number"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="instructor-innercontent">	
							<span><p>Please enter the main Question:</p></span>
							<input type="text" name="mainques" class="instrcutor-inputbox" placeholder="Enter Text"/>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="instructor-innercontent">	
							<span><p>Please enter the Question description:</p></span>
							<input type="text" name="quesdes" class="instrcutor-inputbox" placeholder="Enter Text"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<div class="instructor-innercontent">	
							<span><p>Please enter image path:</p></span>
							<input type="text" name="imgpath" class="instrcutor-inputbox" placeholder="Enter Path"/>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="instructor-innercontent">	
							<span><p>Please enter hint for this question:</p></span>
							<input type="text" name="hint" class="instrcutor-inputbox" placeholder="Enter Text"/>
						</div>
					</div>
				</div>
				
				<div id="subquestion1" >

					<div class="instructor-innercontent" style="padding:2em 2em 0.1em 2em">	
						<div class="row">
							<div class="col-sm-6">
								<p style="display:inline-block; float:left; font-size:1.5em;"><strong>Sub Question</strong></p> 
							</div>
							<div class="col-sm-6">
								<p style="display:inline-block; float:right;">
									<a href="#" class="bttn-small" style="background:#8fc800" onclick="addsubques()">Add</a> <a href="#" class="bttn-small" style="background:#f06060" onclick="delsubques(1)">Delete</a>
								</p>
							</div>
						</div>
						<hr>
					</div>

					<div class="row">
						<div class="col-sm-6">	
							<div class="instructor-innercontent">
								<span><p>Please enter sub question:</p></span>
								<input type="text" name="subquestext-1" class="instrcutor-inputbox"  />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="instructor-innercontent">
								<span><p>Please choose question type:</p></span>
								<div class="radio-toolbar" id="options-1" >
									<input type="radio" name="options-1" value="1" id="radio1-1" onchange="questype(1)"> <label for="radio1-1">Range</label>
									<input type="radio" name="options-1" value="2" id="radio2-1" onchange="questype(1)"> <label for="radio2-1">Text</label>
									<input type="radio" name="options-1" value="3" id="radio3-1" onchange="questype(1)"> <label for="radio3-1">Choice</label>
									<input type="radio" name="options-1" value="4" id="radio4-1" onchange="questype(1)"> <label for="radio4-1">DragDrop</label>
						        </div>
							</div>
						</div>
					</div>

					<div class="" id="rangefields1" style="display: none;" >	
						<div class="row">
							<div class="col-sm-6">
								<div class="instructor-innercontent">	
									<span><p>Please enter Max value of the range:</p></span>
									<input type="text" name="rangemax-1" class="instrcutor-inputbox" />
								</div>
							</div>
							<div class="col-sm-6">
								<div class="instructor-innercontent">
									<span><p>Please enter Step value of the range:</p></span>
									<input type="text" name="rangestep-1" class="instrcutor-inputbox" />
								</div>
							</div>
						</div>
					</div>

					<div class="" id="optionfields1" style="display: none;">
						<div class="row">
							<div class="col-sm-3">	
								<div class="instructor-innercontent">
									<span><p>Please enter Option 1:</p></span>
									<input type="text" name="option1-1" class="instrcutor-inputbox" />
								</div>
							</div>
							<div class="col-sm-3">	
								<div class="instructor-innercontent">
									<span><p>Please enter Option 2:</p></span>
									<input type="text" name="option2-1" class="instrcutor-inputbox" />
								</div>
							</div>
							<div class="col-sm-3">	
								<div class="instructor-innercontent">
									<span><p>Please enter Option 3:</p></span>
									<input type="text" name="option3-1" class="instrcutor-inputbox" />
								</div>
							</div>
							<div class="col-sm-3">	
								<div class="instructor-innercontent">
									<span><p>Please enter Option 4:</p></span>
									<input type="text" name="option4-1" class="instrcutor-inputbox" />
								</div>
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="col-sm-6">
							<div class="instructor-innercontent">
								<span><p>Please enter correct answer:</p></span> 
								<input type="text" name="correctans-1" class="instrcutor-inputbox" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="instructor-innercontent">
								<span><p>Please enter +/- value for estimated or 0 for exact answer:</p></span>
								<input type="text" name="estimatedvalue-1" class="instrcutor-inputbox" />
							</div>
						</div>
					</div>

				</div>
			</div>

			<input type="submit" id="submit1" style="display:none;" />
			</form:form>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="instructor-innercontent" style="text-align:center">	
						<button class="bttn-small md-trigger" data-toggle="modal" data-target="#myModal3"><span class="icon-checkmark"></span> submit</button>
					</div>
				</div>
			</div>

		</section>

		<footer class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-6 colmd-6"><p style="text-align:left">
				&copy; 2014 Discovery, Inc. &middot; <a href="#">Privacy</a>
				&middot; <a href="#">Terms</a>
			</p></div>
					<div class="col-xs-6 colmd-6"><p style="text-align:right"><a href="#">Back to top</a></p></div>
				</div>
			</div>
			
		</footer>
		
		<!--Modal-->
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<p>
							<span class="iconwhite-small icon-user"></span> Profile
						</p>
						<hr>
						<div class="profile">
							<p>Do you want to sign out?</p>
						</div>
					</div>
					<div class="modal-footer">
						<a href="${pageContext.request.contextPath}/SignOut" class="bttn" style="text-decoration:none;">Sign out</a>
						<button class="bttn" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

		<div class="modal fade" id="myModal3" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<p style="text-align: center">
							<span class="iconwhite-small icon-checkmark"></span> Do you want to submit this answer?
						</p>
					</div>
					<div class="modal-footer">
						<button class="bttn" data-dismiss="modal" onclick="document.getElementById('submit1').click()">Submit</button>
						<button class="bttn" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

		<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
		<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
		<script type="text/javascript">

			var num = 1;
			var count = 1;
			jQuery(document).ready(function($) {
				$('#user').popover();
				$('.icon-user').tooltip('show');
				$('.icon-user').click(function(){
					$('#useraction').toggle();
				});
			});

			function questype(flag)
        	{
        		var str = $("input[name=options-"+flag+"]:checked").attr('id');
        		var res = str.split("-");
		        if (res[0] == "radio1") {
		        	$('#rangefields'+flag).show();
		        	$('#optionfields'+flag).hide();
				}
				if (res[0] == "radio2") {
					$('#rangefields'+flag).hide();
		        	$('#optionfields'+flag).hide();
				}
				if (res[0] == "radio3") {
					$('#rangefields'+flag).hide();
		        	$('#optionfields'+flag).show();
				}
				if (res[0] == "radio4") {
					$('#rangefields'+flag).hide();
		        	$('#optionfields'+flag).show();
				}
        	}

			function addsubques()
			{
				num++; count++;
				var ni = document.getElementById('quesanssection');
				var newdiv = document.createElement('div');
				var divIdName = 'subquestion'+num;
				newdiv.setAttribute('id',divIdName);
				ni.appendChild(newdiv);


				var rowhtml = '<div class="instructor-innercontent" style="padding:2em 2em 0.1em 2em"><div class="row"><div class="col-sm-6"><p style="display:inline-block; float:left; font-size:1.5em;"><strong>Sub Question</strong></p></div><div class="col-sm-6"><p style="display:inline-block; float:right;"><a href="#" class="bttn-small" style="background:#8fc800" onclick="addsubques()">Add</a> <a href="#" class="bttn-small" style="background:#f06060" onclick="delsubques('+num+')">Delete</a></p></div></div><hr></div><div class="row"><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please enter sub question:</p></span><div id="subquesinput"></div></div></div><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please choose question type:</p></span><div id="subquesradio"></div></div></div></div><div class="" id="rangefields'+num+'" style="display: none;" ><div class="row"><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please enter Max value of the range:</p></span><div id="subquesrange"></div></div></div><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please enter Step value of the range:</p></span><div id="subquesstep"></div></div></div></div></div><div class="" id="optionfields'+num+'" style="display: none;"><div class="row"><div class="col-sm-3"><div class="instructor-innercontent"><span><p>Please enter Option 1:</p></span><div id="subquesoption1"></div></div></div><div class="col-sm-3"><div class="instructor-innercontent"><span><p>Please enter Option 2:</p></span><div id="subquesoption2"></div></div></div><div class="col-sm-3"><div class="instructor-innercontent"><span><p>Please enter Option 3:</p></span><div id="subquesoption3"></div></div></div><div class="col-sm-3"><div class="instructor-innercontent"><span><p>Please enter Option 4:</p></span><div id="subquesoption4"></div></div></div></div></div><div class="row"><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please enter correct answer:</p></span><input type="text" name="correctans-'+num+'" class="instrcutor-inputbox" /></div></div><div class="col-sm-6"><div class="instructor-innercontent"><span><p>Please enter +/- value for estimated or 0 for exact answer:</p></span><input type="text" name="estimatedvalue-'+num+'" class="instrcutor-inputbox" /></div></div></div>';

				document.getElementById(divIdName).innerHTML += rowhtml;

				var subquesinput = '<input type="text" name="subquestext-'+num+'" class="instrcutor-inputbox" placeholder="Enter Text" />';
				$('#subquestion'+num).find('#subquesinput').append(subquesinput);
				
				var subquesradio = '<div class="radio-toolbar" id="options-'+num+'" onchange="questype('+num+')"><input type="radio" name="options-'+num+'" value="1" id="radio1-'+num+'"> <label for="radio1-'+num+'">Range</label><input type="radio" name="options-'+num+'" value="2" id="radio2-'+num+'"> <label for="radio2-'+num+'">Text</label><input type="radio" name="options-'+num+'" value="3" id="radio3-'+num+'"> <label for="radio3-'+num+'">Choice</label><input type="radio" name="options-'+num+'" value="4" id="radio4-'+num+'"> <label for="radio4-'+num+'">DragDrop</label></div>';

				$('#subquestion'+num).find('#subquesradio').append(subquesradio);

				var subquesrange = '<input type="text" name="rangemax-'+num+'" class="instrcutor-inputbox" placeholder="Enter Number"/>';
				$('#subquestion'+num).find('#subquesrange').append(subquesrange);

				var subquesstep = '<input type="text" name="rangestep-'+num+'" class="instrcutor-inputbox" placeholder="Enter Number"/>';
				$('#subquestion'+num).find('#subquesstep').append(subquesstep);

				var subquesoption1 = '<input type="text" name="option1-'+num+'" class="instrcutor-inputbox" placeholder="Enter Text"/>';
				$('#subquestion'+num).find('#subquesoption1').append(subquesoption1);

				var subquesoption2 = '<input type="text" name="option2-'+num+'" class="instrcutor-inputbox" placeholder="Enter Text"/>';
				$('#subquestion'+num).find('#subquesoption2').append(subquesoption2);

				var subquesoption3 = '<input type="text" name="option3-'+num+'" class="instrcutor-inputbox" placeholder="Enter Text"/>';
				$('#subquestion'+num).find('#subquesoption3').append(subquesoption3);

				var subquesoption4 = '<input type="text" name="option4-'+num+'" class="instrcutor-inputbox" placeholder="Enter Text"/>';
				$('#subquestion'+num).find('#subquesoption4').append(subquesoption4);

			}

			function delsubques(flag)
			{	
				if(count === 1){
					alert("There must be atleast 1 sub question!");
				}
				else{
					$('#subquestion'+flag).remove();
					count--;
				}	
			}

		</script>
<%@ include file="footer.jsp"%>