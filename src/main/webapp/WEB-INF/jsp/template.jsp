<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<c:forEach var="bit" items="${bits}" varStatus="counter">
	   <c:choose>
			<c:when test="${bit.optionstype =='RANGE'}"> 
				<div class="quiz-sub-question-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-file"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							<p class="quiz-sub-question">${bit.subquestion}:</p>
							<p class="quiz-sub-description">Drag the button to the estimated value of the given range.</p>
						</div>
					</div>							
				</div>
				<div class="quiz-sub-answer-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-arrow-right2"></span>
						</div>
						<div class="col-md-11 col-sm-11  ">
							your estimated value is <span id="range" >0</span>
							<input type="range" name="${counter.count}" id="bar" min=0 max=${bit.options[0]} value=0 step=${bit.options[1]} onchange="showValue(this.value);"  />
						</div>
					</div>
				</div>
				<br>
			</c:when>
			<c:when test="${bit.optionstype == 'TEXT'}"> 
				<div class="quiz-sub-question-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-file"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							<p class="quiz-sub-question">${bit.subquestion}:</p>
							<p class="quiz-sub-description">Enter the estimated value in the inputbox given below.</p>
						</div>
					</div>							
				</div>
				<div class="quiz-sub-answer-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-arrow-right2"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							your estimated value is 
							<input type="text" name="${counter.count}" class="quiz-inputbox" id="text" />
						</div>
					</div>
				</div>
				<br>
			</c:when>
			<c:when test="${bit.optionstype == 'CHOICE'}"> 
				<div class="quiz-sub-question-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-file"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							<p class="quiz-sub-question">${bit.subquestion}:</p>
							<p class="quiz-sub-description">Select the estimated value from the choices given below.</p>
						</div>
					</div>							
				</div>
				<div class="quiz-sub-answer-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-arrow-right2"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							<div class="quiz-radio-section">
								<input type="radio" name="${counter.count}" value="${bit.options[0]}" id="radio1-${counter.count}"> <label for="radio1-${counter.count}"><strong>A</strong> <span>${bit.options[0]}</span></label>
								<input type="radio" name="${counter.count}" value="${bit.options[1]}" id="radio2-${counter.count}"> <label for="radio2-${counter.count}"><strong>B</strong> <span>${bit.options[1]}</span></label>
								<input type="radio" name="${counter.count}" value="${bit.options[2]}" id="radio3-${counter.count}"> <label for="radio3-${counter.count}"><strong>C</strong> <span>${bit.options[2]}</span></label>
								<input type="radio" name="${counter.count}" value="${bit.options[3]}" id="radio4-${counter.count}"> <label for="radio4-${counter.count}"><strong>D</strong> <span>${bit.options[3]}</span></label>
					        </div>
						</div>
					</div>
				</div>
				<br>
			</c:when>
			<c:when test="${bit.optionstype == 'DRAGDROP'}"> 
				<div class="quiz-sub-question-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-file"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">
							<p class="quiz-sub-question">${bit.subquestion}: </p>
							<p class="quiz-sub-description">Drag your estimated value into the box below.</p>
						</div>
					</div>							
				</div>
				<div class="quiz-sub-answer-section">
					<div class="row">
						<div class="col-md-1 col-sm-1 hidden-xs">
							<span class="quiz-icon icon-arrow-right2"></span>
						</div>
						<div class="col-md-11 col-sm-11 ">	
							Your answer is <span id="dragdropanswer"></span>
							<div id="dragdropzone">
								<div id="yes-drop" class="drag-drop">${bit.options[0]}</div>
								<div id="yes-drop" class="drag-drop">${bit.options[1]}</div>
								<div id="yes-drop" class="drag-drop">${bit.options[2]}</div>
								<div id="yes-drop" class="drag-drop">${bit.options[3]}</div>
								<br><br>
								<div id="inner-dropzone" class="dropzone">Drag here</div>
							</div>									
						</div>
					</div>
				</div>
		        <input type="hidden" name="${counter.count}" id="dragsans" />
		        <script src="<spring:url value="/resources/js/interact.min.js" />"></script>
		        <script src="<spring:url value="/resources/js/dragdrop.js" />"></script>
				<br>
			</c:when>
		</c:choose>
	</c:forEach>
	
	