<%@ include file="header.jsp"%>

<body class="homepage">
	<div class="wrap">
		<header>
			<div class="container-fluid">
				<div class="row">
					<div class=""
						style="text-align: right; padding: 1em; font-size: 2em;">
						<form:form method="post" action="Profile">
						<input type="submit" id="submit" style="display:none;" />
						<span id="useraction" style="display: none;">
							<a href="#" onclick="document.getElementById('submit').click()" class="bttn-small" style="text-decoration:none;"><span class="icon-user"></span> Profile</a>
							<a href="#" class="bttn-small" style="text-decoration:none;" class"md-trigger" data-toggle="modal" data-target="#myModal3"><span class="icon-checkmark"></span> Sign Out</a>
						</span>
						<a href="#" ><p class="side-icon"><span class="icon-small icon-user icon-effect" data-toggle="tooltip" data-placement="left" title="Welcome, ${sessionScope.user.name}"></span></p></a>
						</form:form>
					</div>
				</div>
			</div>
			<div class="page-top hidden-sm hidden-xs">
				<h1 class="animated fadeInUp">
					<strong>Discovery</strong> <span>Lab</span>
				</h1>
				<p class="animated fadeInDown">An interactive Lab with numerous
					questions that helps honing the skills of the students</p>
				<br>
				<p class="scroll">Scroll Down</p>
			</div>
			
			<div class="page-top-home visible-sm visible-xs">
				<h1 class="animated fadeInUp"><strong>Discovery</strong> <span>Lab</span> </h1>
				<p class="animated fadeInDown hidden-xs">An interactive Lab with numerous questions that helps honing the skills of the students	</p>
				<br>
			</div>
		</header>

		<div class="visible-sm visible-xs" > 

			<section style="background:#fff; padding:3em;">
				<div class="container-fluid">
					<div class="row">
						<div class="sectionleft col-sm-8 col-xs-8">
							<p style="color:#555; font-weight:bold;">Estimating order of Magnitude</p>		
							<p class="hidden-xs">This focuses on teaching students how to think about orders of magnitude and to make estimations. They will be awarded points, based on how closer their estimates are to the correct answer.</p>					
						</div>
						<div class="col-sm-4 col-xs-4">
							<form:form method="post" action="Lab1">
							<input type="submit" id="submit1" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit1').click()" style="text-decoration:none;" class="bttn">Start</a>
							</form:form>
						</div>
					</div>
				</div>
			</section>

			<br>

			<section style=" padding:3em;">
				<div class="container-fluid">
					<div class="row">
						<div class="sectionleft col-sm-8 col-xs-8">
							<p style="color:#fff; font-weight:bold;">What does it take to make water?</p> 
							<p class="hidden-xs">In this lab, students will explore the phases of water and energy needs involved in producing a liquid from 2 gases. They will be awarded points, based on how closeness to the correct answer.</p>
						</div>
						<div class="col-sm-4 col-xs-4">
							<form:form method="post" action="Lab2">
							<input type="submit" id="submit2" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit2').click()" style="text-decoration:none;" class="bttn">Start</a>
							</form:form>
						</div>
					</div>
				</div>
			</section>

			<section style="background:#fff;  padding:3em;">
				<div class="container-fluid">
					<div class="row">
						<div class="sectionleft col-xs-8 col-sm-8">
							<p style="color:#555; font-weight:bold;">Solar Insolence</p> 
							<p class="hidden-xs">In this lab, students will explore the efficiency of solar collection. Students will consider the impact of angles, cloud cover etc. while trying to determine the max efficiency of solar devices.</p>
						</div>
						<div class="col-xs-4 col-sm-4">
							<form:form method="post" action="Lab3">
							<input type="submit" id="submit3" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit3').click()" style="text-decoration:none;" class="bttn">Start</a>
							</form:form>
						</div>
					</div>
				</div>
			</section>

		</div>
		
		<div class="hidden-sm hidden-xs"> 
			<section class="ss-style-triangles pagesection1">
				<div class="container-fluid psv1">
					<div class="row">
						<div class="sectionleft col-sm-8 col-xs-8">
							<h2>Estimating order of Magnitude</h2>
							<p>This focuses on teaching students how to think about orders
								of magnitude and to make estimations. They will be awarded
								points, based on how closer their estimates are to the correct
								answer.</p>
						</div>
						<div class="col-sm-4 col-xs-4">
							<form:form method="post" action="Lab1">
							<input type="submit" id="submit1" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit1').click()" style="text-decoration: none;"><span
								class="iconwhite icon-tools icon-effect iconwhite-effect"></span></a>
							</form:form>
						</div>
					</div>
				</div>
			</section>
	
			<section class=" pagesection2">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<form:form method="post" action="Lab2">
							<input type="submit" id="submit2" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit2').click()" style="text-decoration: none;"><span
								class="icon icon-rainy2 icon-effect"></span></a>
							</form:form>
						</div>
						<div class="sectionright col-sm-8 col-xs-8">
							<h2>What does it take to make water?</h2>
							<p>In this lab, students will explore the phases of water and
								energy needs involved in producing a liquid from 2 gases. They
								will be awarded points, based on how closeness to the correct
								answer.</p>
						</div>
					</div>
				</div>
			</section>
	
			<section class="pagesection3">
				<div class="container-fluid">
					<div class="row">
						<div class="sectionleft col-xs-8 col-sm-8">
							<h2>Solar Insolence</h2>
							<p>In this lab, students will explore the efficiency of solar
								collection. Students will consider the impact of angles, cloud
								cover etc. while trying to determine the max efficiency of solar
								devices.</p>
						</div>
						<div class="col-xs-4 col-sm-4">
							<form:form method="post" action="Lab2">
							<input type="submit" id="submit3" style="display:none;" />
							<a href="#" onclick="document.getElementById('submit3').click()" style="text-decoration: none;"><span
								class="iconwhite icon-cloudy icon-effect iconwhite-effect"></span></a>
							</form:form>
						</div>
					</div>
				</div>
			</section>
	
			<section class="ss-style-triangles pagesection4">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-4 col-sm-4">
							<a href="Lab1" style="text-decoration: none;"><span
								class="icon icon-tools icon-effect"></span></a>
						</div>
						<div class="col-xs-4 col-sm-4">
							<a href="Lab2" style="text-decoration: none;"><span
								class="icon icon-rainy2 icon-effect"></span></a>
						</div>
						<div class="col-xs-4 col-sm-4">
							<a href="Lab3" style="text-decoration: none;"><span
								class="icon icon-cloudy icon-effect"></span></a>
						</div>
					</div>
				</div>
			</section>
	
	
			<svg id="clouds" xmlns="http://www.w3.org/2000/svg" version="1.1"
				width="100%" height="100" viewBox="0 0 100 100"
				preserveAspectRatio="none">
				<path
					d="M-5 100 Q 0 20 5 100 Z
						 M0 100 Q 5 0 10 100
						 M5 100 Q 10 30 15 100
						 M10 100 Q 15 10 20 100
						 M15 100 Q 20 30 25 100
						 M20 100 Q 25 -10 30 100
						 M25 100 Q 30 10 35 100
						 M30 100 Q 35 30 40 100
						 M35 100 Q 40 10 45 100
						 M40 100 Q 45 50 50 100
						 M45 100 Q 50 20 55 100
						 M50 100 Q 55 40 60 100
						 M55 100 Q 60 60 65 100
						 M60 100 Q 65 50 70 100
						 M65 100 Q 70 20 75 100
						 M70 100 Q 75 45 80 100
						 M75 100 Q 80 30 85 100
						 M80 100 Q 85 20 90 100
						 M85 100 Q 90 50 95 100
						 M90 100 Q 95 25 100 100
						 M95 100 Q 100 15 105 100 Z">
				</path>
			</svg>
			<footer class="footer">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-6 colmd-6">
							<p style="text-align: left">
								&copy; 2014 Discovery, Inc. &middot; <a href="#">Privacy</a>
								&middot; <a href="#">Terms</a>
							</p>
						</div>
						<div class="col-xs-6 colmd-6">
							<p style="text-align: right">
								<a href="#">Back to top</a>
							</p>
						</div>
					</div>
				</div>
	
			</footer>
		</div>
		
		<!--Modal-->
		<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<p><span class="iconwhite-small icon-user"></span> Sign Out</p>
						<hr>
						<div class="profile">
							<p>Do you want to sign out?</p>
						</div>
					</div>
					<div class="modal-footer">
						<a href="${pageContext.request.contextPath}/SignOut" class="bttn" style="text-decoration:none;">Sign out</a>
		            	<button class="bttn" data-dismiss="modal">Cancel</button>
		          	</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
	</div>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.icon-user').tooltip('show');
			$('.icon-user').click(function(){
				$('#useraction').toggle();
			});
		});

		$(document).scroll(
				function() {
					$('.pagesection1').toggleClass(
							'pagesectionvisibility animated bounceInRight',
							$(this).scrollTop() > 1);
					$('.pagesection2').toggleClass(
							'pagesectionvisibility animated bounceInLeft',
							$(this).scrollTop() > 500);
					$('.pagesection3').toggleClass(
							'pagesectionvisibility animated bounceInRight',
							$(this).scrollTop() > 900);
					$('.pagesection4').toggleClass(
							'pagesectionvisibility animated bounceInLeft',
							$(this).scrollTop() > 1350);
					$('.scroll').toggleClass('animated fadeOut',
							$(this).scrollTop() > 1);
				});
	</script>
	<%@ include file="footer.jsp"%>