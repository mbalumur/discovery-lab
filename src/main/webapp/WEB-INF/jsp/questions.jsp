<%@ include file="header.jsp"%>
<body class="quiz-page" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
	<header class="container-fluid quiz-header">
		<div class="row">
			<div class="col-md-6 col-sm-3 header-left"><span class="icon-small icon-file"> </span><strong>LAB 1</strong> <span>Estimating order of magnitude</span></div>
			<div class="col-md-6 col-sm-9 header-right hidden-xs">
				<span><a href="#" class="md-trigger" data-toggle="modal"
								data-target="#myModal4"style="text-decoration:none;"><strong>Help</strong></a></span>
				<span><a href="#" style="text-decoration:none;"><strong>${sessionScope.user.name} <span class="icon-small icon-user" style="font-size:1em;"></span></strong></a></span>					
			</div>
		</div>
	</header>
	
	<input type="text" id="timer" value="${timer}"  style="display:none"/>
	
	<section class="quiz-wrap">
		<div class="container-fluid">
			<div class="quiz-progress">
				<div class="row"> 
					<div class="col-md-10">
						<span><strong>Progress</strong></span>
						<span style="float:right;"><strong>${quescount}</strong></span>
						<progress max="100" value="${sessionScope.progress}" class="prog" data-toggle="tooltip"
						data-placement="bottom" title="${sessionScope.progress}% Progress"> </progress>
					</div>	
					<div class="col-md-2 hidden-sm hidden-xs" style="font-size: 2em; text-align:center; line-height:1em;">
						<button class="quiz-btn col-md-12 md-trigger" data-toggle="modal"
								data-target="#myModal3">Submit</button>
					</div>					
				</div>
			</div>
			<div class="quiz-question-section">
				<div class="row">
					<div class="col-md-8">
						<p><strong>Question ${ques.questionid}</strong></p>
						<p class="quiz-question">${ques.question}</p>
						<p class="quiz-description">${ques.description}</p>
						<br>
						<button class="quiz-btn-lite" class="bttn-small md-trigger" data-toggle="modal"
								data-target="#myModal2">
								<span class="icon-bookmark"></span><span> Hint</span></button>
					</div>
					<div class="col-md-4">
						<div class="quiz-image hidden-sm hidden-xs"> 
							<c:set var="images" value="${ques.image}"/>
							<c:set var="image" value="${fn:split(images, '~')}" />
							<c:choose>
								<c:when test="${fn:length(image) gt 1}">
									<img src="${image[0]}" class="blacknwhite col-md-12" onmouseover="this.src='${image[1]}'" onmouseout="this.src='${image[0]}'" />
								</c:when>
								<c:otherwise>
									<img src="${image[0]}" class="blacknwhite" />
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${not empty error}">
				<div class="error" id="error">
				<div class="row">
					<div class="col-md-12">
						<span class="icon-small icon-x" style="font-size:1em;"></span> ${error}
					</div>
				</div>							
			</div>
			</c:if>
			
			<br>
			
			<div class="row">
				<div class="col-md-8">
					
					<form:form method="post"  name="myForm" action="${pageContext.request.contextPath}/Lab${ques.labid}/${ques.questionid+1}">
						
						<%@ include file="template.jsp"%>
						
						<input type="hidden" name="seconds" id="sec" />
						<input type="submit" id="submit1" style="display:none;" />
		   			</form:form>
				</div>

				<div class="col-md-4">
					<div class="quiz-timer-section">
						<div class="row">
							<div class="col-md-3">
								<span class="quiz-icon icon-clock3"></span> clock
							</div>
							<div class="col-md-9">
								<div class="quiz-num">
									<span style="font-size:2.5em; line-height:1em;"><label id="minutes">00</label>:<label id="seconds">00</label></span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="quiz-timer-section">
						<div class="row">
							<div class="col-md-3">
								<span class="quiz-icon icon-pencil"></span> score
							</div>
							<div class="col-md-9">
								<div class="quiz-num">
									<span style="font-size:2.5em; line-height:1em;">${sessionScope.score}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="quiz-btn-smallscreen  hidden-md hidden-lg">
						<div class="row">
							<div class="col-md-12">
								<button class="quiz-btn col-md-12 md-trigger" data-toggle="modal"
								data-target="#myModal3">Submit</button>
							</div>
						</div>	
					</div>	
					<br>
				</div>
			</div>
		</div>
	</section>
	
	<br>
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-6 colmd-6"><p style="text-align:left">
			&copy; 2014 Discovery, Inc. &middot; <a href="#">Privacy</a>
			&middot; <a href="#">Terms</a>
		</p></div>
				<div class="col-xs-6 colmd-6"><p style="text-align:right"><a href="#">Back to top</a></p></div>
			</div>
		</div>
		
	</footer>

	<!--Modal-->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>
						<span class="iconwhite-small icon-user"></span> Profile
					</p>
					<hr>
					<div class="profile">
						<p>Do you want to sign out?</p>
					</div>
				</div>
				<div class="modal-footer">
					<a href="${pageContext.request.contextPath}/SignOut" class="bttn" style="text-decoration:none;">Sign out</a>
					<button class="bttn" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>
						<span class="iconwhite-small icon-bookmark"></span> <strong>Hint</strong> 
					</p>
					<hr> 
					<p style="color:#555; padding: 0.5em 0 0 1em;">${ques.hint}</p>
				</div>
				<div class="modal-footer">
					<button class="bttn" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>
						<span class="iconwhite-small icon-checkmark"></span> <strong>Confirmation</strong> 
					</p>
					<hr>
					<p style="color:#555; padding: 0.5em 0 0 1em;">Do you want to submit this answer?</p>
				</div>
				<div class="modal-footer">
					<button class="bttn" data-dismiss="modal" onclick="doSubmit()">Submit</button>
					<button class="bttn" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>
						<span class="iconwhite-small icon-bookmark"></span> <strong>Help</strong> 
					</p>
					<hr> 
					<p style="color:#555; padding: 0.5em 0 0 1em;">Will be added soon...</p>
				</div>
				<div class="modal-footer">
					<button class="bttn" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>

	<script type="text/javascript">

	
		/*****Answer submission*****/
		function doSubmit() {
			localStorage.clear();
			document.getElementById('submit1').click();
		};
		
		jQuery(document).ready(function($) {
			$('.prog').tooltip('hide');
		});

		/*****slider value change*****/
		function showValue(newValue) {
			document.getElementById("range").innerHTML = newValue;
		}
		/*****Slider color change*****/
		$('input[type="range"]')
				.change(
						function() {
							var val = ($(this).val() - $(this).attr('min'))
									/ ($(this).attr('max') - $(this)
											.attr('min'));
							$(this).css(
									'background-image',
									'-webkit-gradient(linear, left top, right top, '
											+ 'color-stop(' + val
											+ ', #94A14E), ' + 'color-stop('
											+ val + ', #C5C5C5)' + ')');
						});
		
		/******timer ******/

		var minutesLabel = document.getElementById("minutes");
		var secondsLabel = document.getElementById("seconds");		
		var totalSeconds;
		
		if (localStorage.getItem("timer") === null) {
			totalSeconds = 0;
			if(document.getElementById("timer").value != "" || document.getElementById("timer").value != null)
			{
				var oldtime = parseInt(document.getElementById("timer").value);
				totalSeconds = totalSeconds + oldtime;
			}
		} else {
			totalSeconds = localStorage.getItem("timer");
		}

		setInterval(setTime, 1000);

		function setTime() {
			++totalSeconds;
			document.getElementById('sec').value = totalSeconds;
			secondsLabel.innerHTML = pad(totalSeconds % 60);
			minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
			localStorage.setItem("timer", totalSeconds);
		}

		function pad(val) {
			var valString = val + "";
			if (valString.length < 2) {
				return "0" + valString;
			} else {
				return valString;
			}
		}

	</script>
	<%@ include file="footer.jsp"%>