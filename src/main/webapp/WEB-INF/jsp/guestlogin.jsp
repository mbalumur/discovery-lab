<%@ include file="header.jsp"%>
<body class="homepage">
	<div class="wrap">

		<section class="login">
			<div class="animated bounceIn">
				<div class="row">
					<div class="loginbox">
						<span class="icon icon-user3 icon-effect hidden-xs"></span>

						<h2>
							<strong>Guest User</strong>
						</h2>
						<hr>
						<form:form method="post" modelAttribute="user" action="HomePage">
							<p>
								<form:input path="name" class="inputbox" id="login"
									placeholder="Enter your name" /> 
							</p>
							<p>
								<form:input path="email" class="inputbox" id="login"
									placeholder="Enter your email" />
							</p>

							<br>
							<input type="submit" id="submit"
								style="text-decoration: none; display: none;" />
							<a href="#" class="bttn"
								onclick="document.getElementById('submit').click()"
								style="text-decoration: none;"><span class="icon-checkmark"></span>
								Login</a>
							<a href="login" class="bttn" style="text-decoration: none;"><span
								class="icon-x"></span> Cancel</a>
						</form:form>
					</div>
				</div>
			</div>
		</section>

	</div>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
		$(window).resize(function() {
			$('.login').css({
				position : 'absolute',
				left : ($(window).width() - $('.login').outerWidth()) / 2,
				top : ($(window).height() - $('.login').outerHeight()) / 2
			});
		});

		// To initially run the function:
		$(window).resize();
	</script>

	<%@ include file="footer.jsp"%>