<%@ include file="header.jsp"%>

<body class="homepage">
	<section class="welcome">
		<div class="container welcomebox">
			<div class="page-top-home">
				<h1 class="animated fadeInUp">
					<strong>Discovery</strong> <span>Lab</span>
				</h1>
				<p class="animated fadeInDown hidden-xs">An interactive Lab with numerous
					questions that helps honing the skills of the students</p>
				<br>
				<br>
				<div class="row">
					<div class="col-sm-6">
						<a href="HomePage" class="bttn" style="text-decoration: none;"><span
							class="icon-user"></span> Login with ASU Account</a>
					</div>
					<div class="col-sm-6">
						<a href="GuestUser" class="bttn" style="text-decoration: none;"><span
							class="icon-user3"></span> Login as Guest for Demo</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
		$(window).resize(function() {
			$('.welcome').css({
				position : 'absolute',
				left : ($(window).width() - $('.welcome').outerWidth()) / 2,
				top : ($(window).height() - $('.welcome').outerHeight()) / 2
			});
		});

		// To initially run the function:
		$(window).resize();
	</script>
<%@ include file="footer.jsp"%>