<%@ include file="header.jsp"%>
<body class="homepage">
	<div class="wrap">

		<section class="login">
			<div class="animated fadeInDown">
				<div class="row">
					<div class="loginbox">
						<h2><strong>Oops, You seem to be <span style="font-size:2em;">lost!</span></strong></h2>
            			<br>
            			<a href="${pageContext.request.contextPath}/login" class="bttn" style="text-decoration: none;"><span class="icon-checkmark"></span> Go back to Login Page</a>
					</div>
				</div>
			</div>
		</section>

	</div>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
		$(window).resize(function() {
			$('.login').css({
				position : 'absolute',
				left : ($(window).width() - $('.login').outerWidth()) / 2,
				top : ($(window).height() - $('.login').outerHeight()) / 2
			});
		});

		// To initially run the function:
		$(window).resize();
	</script>

	<%@ include file="footer.jsp"%>