<%@ include file="header.jsp"%>
<body class="startpage">
	<div class="wrap">
		<div class="row header-profile">
			<div class="col-md-6 col-sm-3 col-xs-3 header-left">
				<strong>Discovery</strong>
			</div>
			<div class="col-md-6 col-sm-9 col-xs-9 header-right">
					<a href="HomePage"><span class="icon-small icon-home icon-effect"></span></a> 
					<a href="#" id="user" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-title="<strong>${sessionScope.user.name}</strong><br>(${sessionScope.user.email})" data-content='<div style="text-align:center; "><button class="bttn-small" style="text-decoration:none; background:#47a3da;" class="bttn-small md-trigger" data-toggle="modal" data-target="#myModal1">Sign out</button> </div>'><span class="icon-small icon-user icon-effect" ></span></a>
			</div>
		</div>
		<div class="container" style="padding: 1em; color: #555;">
			<div style="padding: 0 1em;">
				<h1>
					<span class="icon-user"> </span>Profile
				</h1>
				<br>
			</div>
			<section class="">
				<div class="animated fadeInUp"
					style="background: #fff; padding: 5em; box-shadow: 0 0 5px #aaa; font-size: 1.25em;">

					<strong style="color: #47a3da">Personal Information</strong>
					<hr>
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<p>
								</span>Name
							</p>
							<p>
								</span>Email
							</p>
						</div>
						<div class="col-sm-8 col-xs-8">
							<p>${sessionScope.user.name}</p>
							<p>${sessionScope.user.email}</p>
						</div>
					</div>
					<br> <strong style="color: #47a3da">Lab 1 Information</strong>
					<hr>
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<p>
								</span>Lab Name
							</p>
							<p>
								</span>Score
							</p>
							<p>
								</span>Date
							</p>
						</div>
						<div class="col-sm-8 col-xs-8">
							<p>Estimating order of magnitude</p>
							<p>-</p>
							<p>-</p>
						</div>
					</div>
					<br> <strong style="color: #47a3da">Lab 2 Information</strong>
					<hr>
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<p>
								</span>Lab Name
							</p>
							<p>
								</span>Score
							</p>
							<p>
								</span>Date
							</p>
						</div>
						<div class="col-sm-8 col-xs-8">
							<p>What does it take to make water?</p>
							<p>-</p>
							<p>-</p>
						</div>
					</div>
					<br> <strong style="color: #47a3da">Lab 3 Information</strong>
					<hr>
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<p>
								</span>Lab Name
							</p>
							<p>
								</span>Score
							</p>
							<p>
								</span>Date
							</p>
						</div>
						<div class="col-sm-8 col-xs-8">
							<p>Solar Insolence</p>
							<p>-</p>
							<p>-</p>
						</div>
					</div>
				</div>
			</section>
		</div>

		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 colmd-6">
						<p style="text-align: left">
							&copy; 2014 Discovery, Inc. &middot; <a href="#">Privacy</a>
							&middot; <a href="#">Terms</a>
						</p>
					</div>
					<div class="col-xs-6 colmd-6">
						<p style="text-align: right">
							<a href="#">Back to top</a>
						</p>
					</div>
				</div>
			</div>

		</footer>

		<!--Modal-->
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<p>
							<span class="iconwhite-small icon-user"></span> Profile
						</p>
						<hr>
						<div class="profile">
							<p>Do you want to sign out?</p>
						</div>
					</div>
					<div class="modal-footer">
						<a href="${pageContext.request.contextPath}/SignOut" class="bttn" style="text-decoration:none;">Sign out</a>
						<button class="bttn" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

	</div>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">
	
		$( document ).ready(function() {
			$('#user').popover();
		});

	</script>
	<%@ include file="footer.jsp"%>