<%@ include file="header.jsp"%>
<body class="startpage">
	<div class="wrap">

		<section class="welcome hidden-xs">
			<div class="container welcomebox">
				<div class="row">
					<div class="col-md-4 hidden-sm">
						<a href="#" style="text-decoration:none;"><span class="icon icon-tools icon-effect"></span></a>
					</div>
					<div class="welcomeright col-md-8">
						<h2><strong>Estimating order of Magnitude</strong></h2>
						<p>This focuses on teaching students how to think about orders of magnitude and to make estimations. They will be awarded points, based on how closer their estimates are to the correct answer.</p>

						<p>Terms and conditions:</p>
						<p style="font-size:0.85em;"><span style="color:red;"><b>*</b></span> We will be collecting data through the games that may result in publications and will definitely help us assess student learning and improve future iterations of the course.</p>
						<br>
						<form:form method="post" action="Lab${lid}/start">
						<input type="submit" id="submit" style="display:none;" />
						<a href="#" onclick="document.getElementById('submit').click()" class="bttn" style="text-decoration: none;"><span class="icon-checkmark"></span> I accept the terms and conditions</a>
						</form:form>
					</div>
				</div>
			</div>
		</section>
		
		<section class="visible-xs">
			<div class="container welcomebox">
				<div class="row">
					<div class="" style="padding:2em;">
						<h2><strong>Estimating order of Magnitude</strong></h2>
						<p>This focuses on teaching students how to think about orders of magnitude and to make estimations. They will be awarded points, based on how closer their estimates are to the correct answer.</p>

						<p>Terms and conditions:</p>
						<p style="font-size:0.85em;"><span style="color:red;"><b>*</b></span> We will be collecting data through the games that may result in publications and will definitely help us assess student learning and improve future iterations of the course.</p>
						<br>
						<form:form method="post" action="Lab${lid}/start">
						<input type="submit" id="submit" style="display:none;" />
						<a href="#" onclick="document.getElementById('submit').click()" class="bttn" style="text-decoration: none;"><span class="icon-checkmark"></span> I accept the terms and conditions</a>
						</form:form>
					</div>
				</div>
			</div>
		</section>

	</div>
	<script src="<spring:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js" />"></script>
	<script type="text/javascript">

		localStorage.clear();

		$(window).resize(function() {
			$('.welcome').css({
				position : 'absolute',
				left : ($(window).width() - $('.welcome').outerWidth()) / 2,
				top : ($(window).height() - $('.welcome').outerHeight()) / 2
			});
		});

		// To initially run the function:
		$(window).resize();
	</script>
	<%@ include file="footer.jsp"%>