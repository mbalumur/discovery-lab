function allowDrop(ev)
{
	ev.preventDefault();
}

function drag(ev)
{
	ev.dataTransfer.setData("Text",ev.target.id);
}

function drop(ev)
{
	ev.preventDefault();
	if(answerbox.innerHTML == 'Answer' || answerbox.innerHTML == '')
	{
		answerbox.innerHTML = "";
		var data=ev.dataTransfer.getData("Text");
		ev.target.appendChild(document.getElementById(data));
	}
	
}

function dropques(ev)
{
	ev.preventDefault();
	var data=ev.dataTransfer.getData("Text");
	document.getElementById("optionbox").appendChild(document.getElementById(data));

}

interact('.dropzone')
    // enable draggables to be dropped into this
    .dropzone(true)
    // only accept elements matching this CSS selector
    .accept('#yes-drop')
    // listen for drop related events
    .on('dragenter', function (event) {
    	if(document.getElementById('dragdropanswer').innerHTML == '')
    	{
	        var draggableElement = event.relatedTarget,
	            dropzoneElement = event.target;	            

	        // feedback the possibility of a drop
	        dropzoneElement.classList.add('drop-target');
	        draggableElement.classList.add('can-drop');
	        //draggableElement.textContent = 'Dragged in';
	        document.getElementById('dragdropanswer').innerHTML = draggableElement.innerHTML;
	        document.getElementById('dragsans').value = draggableElement.innerHTML;
	    }
    })
    .on('dragleave', function (event) {
        // remove the drop feedback style
        event.target.classList.remove('drop-target');
        event.relatedTarget.classList.remove('can-drop');
        document.getElementById('dragdropanswer').innerHTML = ''
        //event.relatedTarget.textContent = 'Dragged out';
    })
    .on('drop', function (event) {
	        //event.relatedTarget.textContent = 'Dropped';
    });

interact('.drag-drop')
    .draggable({
        onmove: function (event) {
            var target = event.target;

            target.x = (target.x|0) + event.dx;
            target.y = (target.y|0) + event.dy;

            target.style.webkitTransform = target.style.transform =
                'translate(' + target.x + 'px, ' + target.y + 'px)';
        }
    })
    .inertia(false)
    .restrict({ drag: 'parent' });