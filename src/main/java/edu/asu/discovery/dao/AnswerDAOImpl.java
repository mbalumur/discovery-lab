package edu.asu.discovery.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.asu.discovery.model.Answer;

@Repository
public class AnswerDAOImpl implements AnswerDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void insertanswer(Answer ans) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("getanswer");
		query.setParameter("uniquesid", ans.getUniquesid());
		query.setParameter("studentid", ans.getStudentid());
		query.setParameter("attemptcount", ans.getAttemptcount());
		System.out.println("size of list is: "+query.list().size());
		
		if(query.list().size() == 0) {
			session.save(ans);
		}
	}

	@Override
	public void updateanswer(Answer ans) {
		Session session = sessionFactory.getCurrentSession();
		session.update(ans);
	}

}
