package edu.asu.discovery.dao;

import edu.asu.discovery.model.User;

public interface UserDAO {
	public void addUser(User user);
	public User getUser(int id);
}
