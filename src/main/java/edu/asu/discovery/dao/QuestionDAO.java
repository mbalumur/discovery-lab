package edu.asu.discovery.dao;

import java.util.List;

import edu.asu.discovery.model.Question;

public interface QuestionDAO {
	List<Question> getQuestions(int labid); 
	void saveQuestion(Question question);
}
