package edu.asu.discovery.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.asu.discovery.model.Question;

@Repository
public class QuestionDAOImpl implements QuestionDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Question> getQuestions(int labid) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("getquestion");
		query.setParameter("labid", labid);
		System.out.println("size of list is: "+query.list().size());
		List<Question> list = query.list();
		return list;
	}

	@Override
	public void saveQuestion(Question question) {
		Session session = sessionFactory.getCurrentSession();
		session.save(question);
	}

}
