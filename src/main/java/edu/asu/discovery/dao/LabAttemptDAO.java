package edu.asu.discovery.dao;

import edu.asu.discovery.model.LabAttempt;

public interface LabAttemptDAO {
	LabAttempt getattempt(int labid, int studentid);
	void setattempt(LabAttempt attempt);
	void updateattempt(LabAttempt attempt);
}
