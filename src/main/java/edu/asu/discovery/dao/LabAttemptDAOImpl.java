package edu.asu.discovery.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.asu.discovery.model.LabAttempt;

@Repository
public class LabAttemptDAOImpl implements LabAttemptDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public LabAttempt getattempt(int labid, int studentid) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.getNamedQuery("getattempt");
		query.setParameter("labid", labid);
		query.setParameter("studentid", studentid);
		System.out.println("size of list is: "+query.list().size());
		
		if(query.list().size() == 0) {
			return null;
		}
		
		List<?> list = query.list();
		return (LabAttempt)list.get(0);
	}

	@Override
	public void setattempt(LabAttempt attempt) {
		Session session = sessionFactory.getCurrentSession();
		session.save(attempt);
	}

	@Override
	public void updateattempt(LabAttempt attempt) {
		Session session = sessionFactory.getCurrentSession();
		session.update(attempt);
	}

}
