package edu.asu.discovery.controller;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import edu.asu.discovery.model.Question;
import edu.asu.discovery.model.User;
import edu.asu.discovery.service.QuestionService;
import edu.asu.discovery.service.UserService;

@Controller
public class MainController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView loginpage() {
		ModelAndView modelAndView = new ModelAndView("login");
		return modelAndView;
	}
	
	@RequestMapping(value="/GuestUser", method = RequestMethod.GET)
	public ModelAndView guestloginpage() {
		ModelAndView modelAndView = new ModelAndView("guestlogin");
		User user = new User();
		modelAndView.addObject("user", user);
		return modelAndView;
	}
	
	@RequestMapping(value="/HomePage", method = RequestMethod.POST)
	public ModelAndView postindexpage(@ModelAttribute("user") User user, HttpSession session) {
		if(user.getName().isEmpty())
		{
			ModelAndView modelAndView = new ModelAndView("login");
			return modelAndView;
		}
		
		Random rnd = new Random();
		int userid = 100000000 + rnd.nextInt(900000000);
		
		user.setUserid(userid);
		userService.addUser(user);
		
		session.setAttribute("user", user);
		ModelAndView modelAndView = new ModelAndView("index");
		System.out.println("Name"+user.getName());
		return modelAndView;
	}
	
	@RequestMapping(value="/HomePage", method = RequestMethod.GET)
	public ModelAndView getindexpage(HttpSession session) {
		if(session.getAttribute("user") == null){
			ModelAndView modelAndView = new ModelAndView("login");
			return modelAndView;
		}
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}
	
	@RequestMapping(value="/Profile", method = RequestMethod.POST)
	public ModelAndView profilepage() {
		ModelAndView modelAndView = new ModelAndView("profile");
		
		System.out.println("Name");
		return modelAndView;
	}

	@RequestMapping(value="/Lab{lid}", method = RequestMethod.POST)
	public ModelAndView startlab(@PathVariable int lid) {
		ModelAndView modelAndView = new ModelAndView("startpage");
		modelAndView.addObject("lid", lid);
		return modelAndView;
	}
	
	@RequestMapping(value="/SignOut", method = RequestMethod.GET)
	public ModelAndView signOut(SessionStatus status, HttpSession session) {
		status.setComplete();
		session.invalidate();
		ModelAndView modelAndView = new ModelAndView("signout");
		return modelAndView;
	}

}
