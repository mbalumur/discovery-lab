package edu.asu.discovery.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.asu.discovery.model.Bit;
import edu.asu.discovery.model.Bit.OptionType;
import edu.asu.discovery.model.Bits;
import edu.asu.discovery.model.Question;
import edu.asu.discovery.service.QuestionService;

@Controller
public class InstructorController {
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(value="/Admin", method = RequestMethod.GET)
	public ModelAndView loginpage() {
		ModelAndView modelAndView = new ModelAndView("instructor-ques");
		return modelAndView;
	}
	
	@RequestMapping(value="/Admin/Submit", method = RequestMethod.POST)
	public ModelAndView submitanswer(@RequestParam Map<String, String> map) {
		
		int labid = 0;;
		int quesid = 0;
		String question = "";
		String description = "";
		String imagepath = "";
		String hint = "";


		List<Bit> bitslist = new ArrayList<>();
		Bit bit = null;
		List<String> opts = null;
		int currentoption = 0;
		
		Iterator it = map.entrySet().iterator();
	    while (it.hasNext()) {
	    	
	        Map.Entry pairs = (Map.Entry)it.next();
	        System.out.println(pairs.getKey() + " = " + pairs.getValue());
	        
	        if(pairs.getKey().toString().equals("lab")){
	        	labid = Integer.parseInt(pairs.getValue().toString());
	        	
	        } else if(pairs.getKey().toString().equals("quesno")){
	        	quesid = Integer.parseInt(pairs.getValue().toString());
	        	
	        } else if(pairs.getKey().toString().equals("mainques")){
	        	question = pairs.getValue().toString();
	        	
	        } else if(pairs.getKey().toString().equals("quesdes")){
	        	description = pairs.getValue().toString();
	        	
	        } else if(pairs.getKey().toString().equals("imgpath")){
	        	imagepath = pairs.getValue().toString();
	        	
	        } else if(pairs.getKey().toString().equals("hint")){
	        	hint = pairs.getValue().toString();
	        	
	        } else if(pairs.getKey().toString().contains("subquestext") ){
	        	bit = new Bit();
	        	bit.setSubquestion(pairs.getValue().toString());
	        	
	        } else if(pairs.getKey().toString().contains("options")){
	        	currentoption = Integer.parseInt(pairs.getValue().toString());
	        	if(currentoption == 1){
	        		bit.setOptionstype(OptionType.RANGE);
	        		opts = new ArrayList<>();
	        		
		        } else if(currentoption == 2){
	        		bit.setOptionstype(OptionType.TEXT);
	        		opts = new ArrayList<>();
	        		
		        } else if(currentoption == 3){
	        		bit.setOptionstype(OptionType.CHOICE);
	        		opts = new ArrayList<>();
	        		
		        } else if(currentoption == 4){
	        		bit.setOptionstype(OptionType.DRAGDROP);
	        		opts = new ArrayList<>();
		        }	        	
	        	
	        } else if(currentoption == 1 && (pairs.getKey().toString().contains("rangemax") || pairs.getKey().toString().contains("rangestep"))){
		       	opts.add(pairs.getValue().toString());
		       	
	        } else if(currentoption == 3 && (pairs.getKey().toString().contains("option1") || pairs.getKey().toString().contains("option2") || pairs.getKey().toString().contains("option3") || pairs.getKey().toString().contains("option4"))){
		       	opts.add(pairs.getValue().toString());
		       	
	        } else if(currentoption == 4 && (pairs.getKey().toString().contains("option1") || pairs.getKey().toString().contains("option2") || pairs.getKey().toString().contains("option3") || pairs.getKey().toString().contains("option4"))){
		       	opts.add(pairs.getValue().toString());
		       	
	        } else if(pairs.getKey().toString().contains("correctans")){
	        	bit.setCorrectanswer(pairs.getValue().toString());
	        	
	        } else if(pairs.getKey().toString().contains("estimatedvalue")){
	        	bit.setAnswerrange(Integer.parseInt(pairs.getValue().toString()));
	        	
	        	bit.setOptions(opts);
	        	bitslist.add(bit);
	        	currentoption = 0;
	        }
	        
	        it.remove();
	    } 
	    
	    Bits bits = new Bits();
	    
	    bits.setBits(bitslist);
	    
	    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	    String json = null;
		try {
			json = ow.writeValueAsString(bits);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(json);
		
		Bits jsonobj = null;
		try {
			jsonobj = new ObjectMapper().readValue(json, Bits.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(jsonobj);
	        
	    Question ques = new Question();
	    ques.setLabid(labid);
	    ques.setQuestionid(quesid);
	    ques.setQuestion(question);
	    ques.setDescription(description);
	    ques.setImage(imagepath);
	    ques.setHint(hint);
	    ques.setBits(json);
	    ques.setBitslist(jsonobj);
	    
	    System.out.println(ques);
	    
	    questionService.saveQuestion(ques);
	    
		return null;
	}

}
