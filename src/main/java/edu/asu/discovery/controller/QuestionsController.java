package edu.asu.discovery.controller;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import edu.asu.discovery.model.Answer;
import edu.asu.discovery.model.Bit;
import edu.asu.discovery.model.Bits;
import edu.asu.discovery.model.LabAttempt;
import edu.asu.discovery.model.Question;
import edu.asu.discovery.model.User;
import edu.asu.discovery.service.AnswerService;
import edu.asu.discovery.service.LabAttemptService;
import edu.asu.discovery.service.QuestionService;
import edu.asu.discovery.service.UserService;

@Controller
public class QuestionsController {

	@Autowired
	private UserService userService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private AnswerService answerService;

	@Autowired
	private LabAttemptService labAttemptService;

	@RequestMapping(value="/Lab{lid}/start", method = RequestMethod.POST)
	public ModelAndView startquestions(@PathVariable int lid, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("questions");
		User user = (User) session.getAttribute("user");
		double score = 0.0;
		double progress = 0.0;
		session.setAttribute("score", score);
		session.setAttribute("progress", progress);

		if(session.getAttribute("labattempt") == null){
			LabAttempt attempt = labAttemptService.getattempt(lid, user.getUserid());

			if(attempt == null){
				attempt = new LabAttempt();
				attempt.setLabid(lid);
				attempt.setStudentid(user.getUserid());
				attempt.setAttempt(1);
				labAttemptService.setattempt(attempt);
				session.setAttribute("labattempt", 1);
			}
			else {
				attempt.setAttempt(attempt.getAttempt()+1);
				labAttemptService.updateattempt(attempt);
				session.setAttribute("labattempt", attempt.getAttempt());
			}
		}

		List<Question> questions = questionService.getQuestions(lid);
		session.setAttribute("questions", questions);

		List<Answer> answers = new ArrayList<Answer>();
		session.setAttribute("answers", answers);
		
		String json = questions.get(0).getBits();
		
		Bits jsonobj = null;
		try {
			jsonobj = new ObjectMapper().readValue(json, Bits.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		modelAndView.addObject("ques", questions.get(0));
		modelAndView.addObject("bits", jsonobj.getBits());
		modelAndView.addObject("timer", 0);
		modelAndView.addObject("quescount", questions.size());
		
		return modelAndView;
	}


	@RequestMapping(value="/Lab{lid}/{qid}", method = RequestMethod.POST)
	public ModelAndView submitanswer(@RequestParam Map<String, String> map, @PathVariable int lid, @PathVariable int qid, HttpSession session) {
		
		String seconds="";
		List<String> useranswer = new ArrayList<>();

		double score = 10.0;

		Iterator it = map.entrySet().iterator();
		int useranswercount = 0;
		
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			System.out.println(pairs.getKey() + " = " + pairs.getValue());

			if(pairs.getKey().equals("seconds")){
				seconds = (String) pairs.getValue();
			}
			else{
				useranswer.add(pairs.getValue().toString());
				if(!pairs.getValue().toString().equals(""))
					useranswercount++;
			}

			it.remove();
		}
		
		System.out.println("useranswercount: " + useranswercount);
		
		List<Question> questions = (List<Question>) session.getAttribute("questions");
		String checkjson = questions.get(qid-2).getBits();
		
		Bits checkjsonobj = null;
		try {
			checkjsonobj = new ObjectMapper().readValue(checkjson, Bits.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Bit> bits = checkjsonobj.getBits();
		
		if(bits.size() != useranswercount){
			
			ModelAndView modelAndView = new ModelAndView("questions");
			modelAndView.addObject("ques", questions.get(qid-2));
			modelAndView.addObject("bits", bits);
			modelAndView.addObject("error", "Please enter all the fields!");
			modelAndView.addObject("timer", seconds);
			modelAndView.addObject("quescount", questions.size());
			
			return modelAndView;
		}
		
		boolean repeatflag = false; 
		List<Answer> answers = (List<Answer>) session.getAttribute("answers");
		
		for(Answer a : answers){
			if(a.getUniquesid() == questions.get(qid-2).getUniqueid()){
				repeatflag=true;
				break;
			}
		}
		
		if(!repeatflag)
		{
			User user = (User) session.getAttribute("user");
			
			double progress = (double)(((qid-1) * 100) / questions.size());
			System.out.println("Progress: " + progress);
			session.setAttribute("progress", progress);
					
			int counter = 0;
			double eachscore = roundTwoDecimals((double) (score/(double)bits.size()));
			System.out.println("eachScore: " + eachscore);
			
			for(Bit bit : bits){
				if(bit.getAnswerrange() == 0){
					if(!bit.getCorrectanswer().equals(useranswer.get(counter))){
						score = roundTwoDecimals(score - eachscore);
						System.out.println("Score after bit " + (counter+1) + ": " + score);
					}				
				}else{
					double range1 = Double.valueOf(bit.getCorrectanswer()) -  Double.valueOf(bit.getAnswerrange());
					double range2 = Double.valueOf(bit.getCorrectanswer()) +  Double.valueOf(bit.getAnswerrange());
					double test = Double.valueOf(useranswer.get(counter));
					if(!(range1 <= test && test <= range2)){
						score = roundTwoDecimals(score - eachscore);
						System.out.println("Score after bit " + (counter+1) + ": " + score);
					}
				}
				counter++;
			}
	
			System.out.println("Score: " + score);
	
			double sessionscore = (double) session.getAttribute("score");
			if(score < 1){
				score = 0.0;
			}
			double totalscore = score + sessionscore;
			session.setAttribute("score", roundTwoDecimals(totalscore));
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		    String ansjson = null;
			try {
				ansjson = ow.writeValueAsString(useranswer);
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			Answer ans = new Answer();
			ans.setLabid(questions.get(qid-2).getLabid());
			ans.setUniquesid(questions.get(qid-2).getUniqueid());
			ans.setStudentid(user.getUserid());
			ans.setAnswerattempt(ansjson);
			ans.setSeconds(Integer.parseInt(seconds));
			ans.setAttemptcount((Integer) session.getAttribute("labattempt"));
			ans.setScore(score);
	
			
			answers.add(ans);
			session.setAttribute("answers", answers);
	
			//answerService.insertanswer(ans);
	
			if(qid == questions.size()+1){
				session.removeAttribute("labattempt");
				session.removeAttribute("score");
				return new ModelAndView("redirect:" +  "/Lab"+lid+"/Report");
			}
		}
		
		
		ModelAndView modelAndView = new ModelAndView("questions");
		int next = qid - 1;
		Question ques = questions.get(next);

		String json = ques.getBits();
		
		Bits jsonobj = null;
		try {
			jsonobj = new ObjectMapper().readValue(json, Bits.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		modelAndView.addObject("ques", ques);
		modelAndView.addObject("bits", jsonobj.getBits());
		modelAndView.addObject("timer", 0);
		modelAndView.addObject("quescount", questions.size());
		
		return modelAndView;
	}

	double roundTwoDecimals(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		return Double.valueOf(twoDForm.format(d));
	}

	@RequestMapping(value="/Lab{lid}/Report")
	public ModelAndView report(@PathVariable int lid, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("report");
		
		User user = (User) session.getAttribute("user");
		
		List<Question> questions = (List<Question>) session.getAttribute("questions");
		int quescount = questions.size();
		
		List<Answer> answers = (List<Answer>) session.getAttribute("answers");
		int anscount = answers.size();

		double score = 0.0;
		int time = 0;		
		
		for(Answer ans : answers){
			score = score + ans.getScore();
			time = time + ans.getSeconds();
			answerService.insertanswer(ans);
		}
		
		int attemptperc = (anscount/quescount) * 100;
		int totalscore = (int) Math.abs(score);
		int totalscoreperc = ((int)((totalscore*100)/(10*quescount))) ;
		int avgtime = time/anscount;
		int avgtimeperc = ((int) Math.abs((avgtime*100)/300));
		
		System.out.println(attemptperc);
		System.out.println(totalscore);
		System.out.println(totalscoreperc);
		System.out.println(avgtime);
		System.out.println(avgtimeperc);
		
		String msg = "";
		if(totalscoreperc >= 50 && totalscoreperc < 75){
			msg = "Well Done";
		} else if(totalscoreperc >= 75 && totalscoreperc < 90){
			msg = "Good Job";
		} else if(totalscoreperc >= 90){
			msg = "Awesome";
		} else {
			msg = "Better try again";
		}

		modelAndView.addObject("attemptperc", attemptperc);
		modelAndView.addObject("attempted", anscount);
		modelAndView.addObject("totalscore", totalscore);
		modelAndView.addObject("totalscoreperc", totalscoreperc);
		modelAndView.addObject("avgtime", avgtime);
		modelAndView.addObject("avgtimeperc", avgtimeperc);
		modelAndView.addObject("msg", msg);
		return modelAndView;
	}
}
