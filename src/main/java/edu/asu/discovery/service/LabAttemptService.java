package edu.asu.discovery.service;

import edu.asu.discovery.model.LabAttempt;

public interface LabAttemptService {
	LabAttempt getattempt(int labid, int studentid);
	void setattempt(LabAttempt attempt);
	void updateattempt(LabAttempt attempt);

}
