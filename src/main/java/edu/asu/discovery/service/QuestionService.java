package edu.asu.discovery.service;

import java.util.List;

import edu.asu.discovery.model.Question;

public interface QuestionService {
	List<Question> getQuestions(int labid); 
	public void saveQuestion(Question question);
}
