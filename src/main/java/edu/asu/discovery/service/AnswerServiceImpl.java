package edu.asu.discovery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.asu.discovery.dao.AnswerDAO;
import edu.asu.discovery.model.Answer;

@Service
public class AnswerServiceImpl implements AnswerService{
	
	@Autowired
	AnswerDAO answerDAO;

	@Override
	@Transactional
	public void insertanswer(Answer ans) {
		answerDAO.insertanswer(ans);
	}

	@Override
	@Transactional
	public void updateanswer(Answer ans) {
		answerDAO.updateanswer(ans);
	}

}
