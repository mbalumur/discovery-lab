package edu.asu.discovery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.asu.discovery.dao.QuestionDAO;
import edu.asu.discovery.model.Question;

@Service
public class QuestionServiceImpl implements QuestionService{

	@Autowired
	QuestionDAO questionDAO;
	
	@Override
	@Transactional
	public List<Question> getQuestions(int labid) {
		return questionDAO.getQuestions(labid);
	}

	@Override
	@Transactional
	public void saveQuestion(Question question) {
		questionDAO.saveQuestion(question);
	}

}
