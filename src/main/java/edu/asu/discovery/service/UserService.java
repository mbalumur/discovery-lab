package edu.asu.discovery.service;

import edu.asu.discovery.model.User;

public interface UserService {
	public void addUser(User user);
	public User getUser(int id);
}
