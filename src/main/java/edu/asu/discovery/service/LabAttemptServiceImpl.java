package edu.asu.discovery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.asu.discovery.dao.LabAttemptDAO;
import edu.asu.discovery.model.LabAttempt;

@Service
public class LabAttemptServiceImpl implements LabAttemptService{
	
	@Autowired
	LabAttemptDAO labattemptDAO;

	@Override
	@Transactional
	public LabAttempt getattempt(int labid, int studentid) {
		return labattemptDAO.getattempt(labid, studentid);
	}

	@Override
	@Transactional
	public void setattempt(LabAttempt attempt) {
		labattemptDAO.setattempt(attempt);
	}

	@Override
	@Transactional
	public void updateattempt(LabAttempt attempt) {
		labattemptDAO.updateattempt(attempt);
	}

}
