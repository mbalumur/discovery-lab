package edu.asu.discovery.model;

import java.util.List;

public class Bit {
	
	public enum OptionType { RANGE, TEXT, CHOICE, DRAGDROP };
	
	private String subquestion;
	private String correctanswer;
	private int answerrange;
	private OptionType optionstype;
	private List<String> options;
	
	public String getSubquestion() {
		return subquestion;
	}
	public void setSubquestion(String subquestion) {
		this.subquestion = subquestion;
	}
	public String getCorrectanswer() {
		return correctanswer;
	}
	public void setCorrectanswer(String correctanswer) {
		this.correctanswer = correctanswer;
	}
	public int getAnswerrange() {
		return answerrange;
	}
	public void setAnswerrange(int answerrange) {
		this.answerrange = answerrange;
	}
	public OptionType getOptionstype() {
		return optionstype;
	}
	public void setOptionstype(OptionType optionstype) {
		this.optionstype = optionstype;
	}
	public List<String> getOptions() {
		return options;
	}
	public void setOptions(List<String> options) {
		this.options = options;
	}
	
	
}
