package edu.asu.discovery.model;

import java.util.List;

public class Bits {
	
	private List<Bit> bits;
	
	public List<Bit> getBits() {
		return bits;
	}

	public void setBits(List<Bit> bits) {
		this.bits = bits;
	}

}

