package edu.asu.discovery.model;

public class Answer {
	private int recordid;
	private int uniquesid;
	private int labid;
	private int studentid;
	private String answerattempt;
	private double score;
	private int seconds;
	private int attemptcount;
	
	public int getRecordid() {
		return recordid;
	}
	public void setRecordid(int recordid) {
		this.recordid = recordid;
	}
	public int getUniquesid() {
		return uniquesid;
	}
	public void setUniquesid(int uniquesid) {
		this.uniquesid = uniquesid;
	}
	public int getLabid() {
		return labid;
	}
	public void setLabid(int labid) {
		this.labid = labid;
	}
	public int getStudentid() {
		return studentid;
	}
	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}
	public String getAnswerattempt() {
		return answerattempt;
	}
	public void setAnswerattempt(String answerattempt) {
		this.answerattempt = answerattempt;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}

	public int getSeconds() {
		return seconds;
	}
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
	public int getAttemptcount() {
		return attemptcount;
	}
	public void setAttemptcount(int attemptcount) {
		this.attemptcount = attemptcount;
	}
	
	
}
