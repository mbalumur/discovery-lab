package edu.asu.discovery.model;


public class Question {
	
	private int uniqueid;
	private int labid;
	private int questionid;
	private String question;
	private String description;
	private String image;
	private String hint;
	private String bits;
	private Bits bitslist;

	public int getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(int uniqueid) {
		this.uniqueid = uniqueid;
	}

	public Bits getBitslist() {
		return bitslist;
	}

	public void setBitslist(Bits bitslist) {
		this.bitslist = bitslist;
	}

	public int getLabid() {
		return labid;
	}

	public void setLabid(int labid) {
		this.labid = labid;
	}

	public int getQuestionid() {
		return questionid;
	}

	public void setQuestionid(int questionid) {
		this.questionid = questionid;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getBits() {
		return bits;
	}

	public void setBits(String bits) {
		this.bits = bits;
	}


	@Override
	public String toString() {
	   return "Question [uniqueid=" + uniqueid + ", labid=" + labid + ", questionid=" + questionid + ", question=" + question + ", description=" + description + ", image=" + image + ", hint=" + hint + ", bits=" + bits + ", bitslist=" + bitslist + "]";
	}
	
}
